FROM osrf/ros:noetic-desktop-full

ENV ROS_DISTRO=noetic

SHELL ["/bin/bash", "-c"]
RUN echo "source /opt/ros/${ROS_DISTRO}/setup.bash" >> ~/.bashrc && \
source ~/.bashrc

RUN mkdir -p ~/catkin_ws/src
RUN apt-get update && apt-get install -y \
python3-catkin-tools vim \
&& rm -rf /var/lib/apt/lists/*

RUN rosdep update
RUN apt-get update && apt-get dist-upgrade -y \
&& apt install -y ros-noetic-moveit \
&& rm -rf /var/lib/apt/lists/*

RUN cd ~/catkin_ws/src

RUN cd ~/catkin_ws \
&& catkin config --extend /opt/ros/${ROS_DISTRO} --cmake-args -DCMAKE_BUILD_TYPE=Release

RUN apt-get update && apt-get install -y ros-noetic-joy ros-noetic-teleop-twist-joy \
  ros-noetic-teleop-twist-keyboard ros-noetic-laser-proc \
  ros-noetic-rgbd-launch ros-noetic-depthimage-to-laserscan \
  ros-noetic-rosserial-arduino ros-noetic-rosserial-python \
  ros-noetic-rosserial-server ros-noetic-rosserial-client \
  ros-noetic-rosserial-msgs ros-noetic-amcl ros-noetic-map-server \
  ros-noetic-move-base ros-noetic-urdf ros-noetic-xacro \
  ros-noetic-compressed-image-transport ros-noetic-rqt* \
  ros-noetic-gmapping ros-noetic-navigation ros-noetic-interactive-markers \
  ros-noetic-moveit-visual*  ros-noetic-libfranka ros-noetic-franka-ros tmux \
  ros-noetic-ros-control ros-noetic-ros-controllers

  RUN apt-get update && apt-get install -y ros-noetic-dynamixel-sdk \
  ros-noetic-turtlebot3-msgs ros-noetic-turtlebot3* 

  RUN apt-get autoclean
  RUN echo "export TURTLEBOT3_MODEL=burger" >> ~/.bashrc && source ~/.bashrc
  RUN echo "source /root/catkin_ws/devel/setup.bash" >> ~/.bashrc && source ~/.bashrc

  RUN cd ~/catkin_ws/ && rosdep install -y --rosdistro $ROS_DISTRO --ignore-src --from-paths src
  
  RUN cd ~/catkin_ws/ && catkin build 
  
  WORKDIR /root